'use strict'
// function definitions
const clearName = function (event) {
  console.log(username.id, username.value);  
  username.value = '';
  username.value = username.defaultValue;
}


const remainingChars = function (event) {
  let element = event.currentTarget;
  console.log(element.id, element.value);
  let remain = element.maxLength - element.value.length;
  remainingOutput.value = remain + ' characters left';
}

const checkForm = function (event) {
  event.preventDefault();
  if ( username.value ) {
    error.classList.remove( 'show' );
    thank.classList.add( 'show' );
    ok.classList.add( 'show' );
    }else {
    error.classList.add( 'show' );
    thank.classList.remove( 'show' );
    ok.classList.add( 'show' );
    username.focus();
    }
};

// variable declarations
let week5Form = document.querySelector( '#survey' );
let username = document.querySelector( '#username' );
let clearButton = document.querySelector( '#clearButton' );
let age = document.querySelector( '#age' );
let donation = document.querySelector( '#donation' );
let donationOutput = document.querySelector( '#donationOutput' );
let color = document.querySelector( '#color' );
let message = document.querySelector( '#message' );
let remainingOutput = document.querySelector( '#remainingOutput' );
let error = document.querySelector( '#error' );
let thank = document.querySelector( '#thank' );
// script initialisation

clearButton.addEventListener( 'click', clearName );
message.addEventListener( 'input', remainingChars );
survey.addEventListener( 'submit', checkForm );
