'use strict';

// function definitions
const selectLink = function ( event ) {
    console.log( 'clicked on', event.currentTarget );
    highlightLink( event.currentTarget );
    }
const highlightLink = function ( element ) {
    for ( let link of links ) {
        link.classList.remove( 'selected' );
        }
    element.classList.add('selected')        
    }
const checkSections = function(entires){
    for (let entry of entires){
        if ( entry.intersectionRatio >= 0.5 ) {
           
            let hash = '#' + entry.target.id;
            for(let link of links){
                if(link.hash==hash){
                    highlightLink(link);
                }
            }
        }
    }
}      

const scrollToAnimate = function () {
    anim1.style.animationDelay = -scrollAmount( secondpage ) + 's';
    anim2.style.animationDelay = -scrollAmount( secondpage ) + 's';
}
const scrollAmount = function ( element ) {
    let scrollTop = innerHeight + element.getBoundingClientRect().top;
    let scrollHeight = element.offsetHeight + innerHeight;

    let scrollValue = scrollTop / scrollHeight;
    return scrollValue;

}

// variable declarations
let links = document.querySelectorAll( 'nav a' );
let observer = new IntersectionObserver( checkSections, {threshold:[0.5] } );
let sections = document.querySelectorAll('section');

let secondpage = document.querySelector( '#secondpage' );
let anim1 = document.querySelector( '#anim1' );
let anim2 = document.querySelector( '#anim2' );

let bg = document.querySelector('#bg')
let UFO = document.querySelector('#UFO')
let museum = document.querySelector('#museum')
let light = document.querySelector('#light')
let cloud = document.querySelector('#cloud')
let text = document.querySelector('#text')

let logo = document.querySelector('.logo')

// script initialisation
AOS.init({
'duration' : 1000, 
'mirror' : true, 
'offset' : 100, 
'once' : false
});
for ( let link of links ) {
    link.addEventListener( 'click', selectLink );
    }
  
document.body
for(let section of sections){
    console.log("Section",section);
    observer.observe(section);
}
document.addEventListener('scroll' ,scrollToAnimate);

imagesLoaded( 'body', { background: 'section' }, function () {
    document.querySelector( '#preloader' ).classList.add( 'loaded' );
    setTimeout( function(){AOS.init( { duration: 1000 } );
    }, 1000 );      
 } );

 window.addEventListener('scroll', () => {
    var value = window.scrollY;
    bg.style.top = value * 0.5 + 'px';
    UFO.style.left = -value * 0.5 + 'px';
    UFO.style.top = value * 1 + 'px';
    museum.style.top = -value * 0.15 + 'px';
    light.style.top = -value * 0.15 + 'px';
    cloud.style.top = value * 0.15 + 'px';
    text.style.top = value * 0.15 + 'px';
   
})

/*jQuery  animation section1 */
$("#light").hide();
$(".indoor").hide();
$("#arrowin").hide();
$(document).ready(function(){
    $("#text").click(function(){ 
    $("#UFO").animate({opacity:1},1000);
    $("#UFO1").animate({opacity:0},1000);
    $("#cloud1").animate({opacity:0},1000);
    $("#cloud").animate({opacity:1},1000);
    $("#bg1").animate({opacity:0},1000);
    $("#bg").animate({opacity:1},1000);
    $("#museum1").animate({opacity:0},1000);
    $("#museum").animate({opacity:1},1000);
    $("#rocket1").remove();
    $("#rocket").animate({opacity:1});
    $("#text").remove();
    $("#light").show();
    $(".indoor").show(500);
    $("#arrowin").show(1000);
    });
    $(".indoor").click(function(){
        $(location).attr("href","#secondpage");
    });
  });


/* jQuery animation section2 */
$("#VR_glasses").hide(1000);
$(".inmuseum").hide();
$("#star1").hide();
$("#star2").hide();
$("#star3").hide();
$("#star4").hide();
$("#glasseslight").hide();
$("#glasseslight2").hide();
$(document).ready(function(){
    $("#boxlight").click(function(){ 
    $("#boxlight").animate({opacity:1},3000);
    $("#upbox").animate({opacity:0},2000);
    $("#step1").animate({opacity:0},100);
    $("#step2").animate({opacity:1},3000);
    $("#VR_glasses").show(2000);
    $("#hidden2").remove();
    $("#star1").show(100);
    $("#star2").show(100);
    $("#star3").show(100);
    $("#star4").show(100);
    $("#glasseslight").show();
    $("#glasseslight2").show();
    });
  });
  $(document).ready(function(){
    $("#VR_glasses").click(function(){ 
    $("#VR_glasses").hide(1000);
    $(".inmuseum").show(1000);
    $("#star1").hide(100);
    $("#star2").hide(100);
    $("#star3").hide(100);
    $("#star4").hide(100);
    $("#glasseslight").hide();
    $("#glasseslight2").hide();
    });
    $(document).ready(function(){
        $(".inmuseum").click(function(){ 
        $(location).attr("href","#thirdpage",1000)
        });
    });
  });
/*jQuery  animation section3 */
$(document).ready(function(){
    $(".lightbutton").click(function(){
        $("#bglight").animate({opacity:1},3000);
        $(".lightbutton").remove();
        $("#hidden").remove();
        $("#p1").remove();
        $("#introbutton").animate({opacity:1},3000);
        $("#p2").animate({opacity:1},3000);
        
    });
    $("#introbutton").click(function(){
        $("#introduction").animate({opacity:1},2000);
        $(".button1").animate({opacity:1},6000);
        $("#hidden_1").remove();
    }); 
    $(".button1").click(function(){
        $(".button2").animate({opacity:1},6000);
        $("#arrow").animate({opacity:1},3000);
        $("#hidden_2").remove();
    });
    $(".button2").click(function(){
        $(location).attr("href","#fourthpage");
});

});
/*jQuery  animation section4 */
$(document).ready(function(){
    $(".lightbutton2").click(function(){
        $("#bglight4").animate({opacity:1},3000);
        $("#intro4").animate({opacity:1},1500);
        $("#normalstyle").animate({opacity:1},3500);
        $(".style1").animate({opacity:1},3500);
        $(".style2").animate({opacity:1},3500);
        $(".style3").animate({opacity:1},3500);
        $(".style4").animate({opacity:1},3500);
        $(".style5").animate({opacity:1},3500);
        $(".changeroom").animate({opacity:1},3500);
        $("#gray").remove();
        $(".lightbutton2").remove();
        $("#hidden4").remove();
    });
    $(".changeroom").click(function(){
        $("#hidden4_1").remove();
        $(".endroom").animate({opacity:1},3500);
        $(".lastroom").animate({opacity:1},3500);
        $("#arrow1").animate({opacity:1},3500);
        $("#arrow2").animate({opacity:1},3500);
    });
    $(".lastroom").click(function(){
        $(location).attr("href","#thirdpage");
    });
    $(".endroom").click(function(){
        $(location).attr("href","#endpage");
    });
    $(".style5").click(function(){
        $("#normalstyle").animate({opacity:1},3500);
        $("#watercolorstyle").animate({opacity:0},3500);
        $("#sketchstyle").animate({opacity:0},3500);
        $("#pixelstyle").animate({opacity:0},3500);
        $("#cartoonstyle").animate({opacity:0},3500);
    });
    $(".style1").click(function(){
        $("#normalstyle").animate({opacity:0},3500);
        $("#watercolorstyle").animate({opacity:1},3500);
        $("#sketchstyle").animate({opacity:0},3500);
        $("#pixelstyle").animate({opacity:0},3500);
        $("#cartoonstyle").animate({opacity:0},3500);
    });
    $(".style2").click(function(){
        $("#normalstyle").animate({opacity:0},3500);
        $("#watercolorstyle").animate({opacity:0},3500);
        $("#sketchstyle").animate({opacity:1},3500);
        $("#pixelstyle").animate({opacity:0},3500);
        $("#cartoonstyle").animate({opacity:0},3500);
    });
    $(".style3").click(function(){
        $("#normalstyle").animate({opacity:0},3500);
        $("#watercolorstyle").animate({opacity:0},3500);
        $("#sketchstyle").animate({opacity:0},3500);
        $("#pixelstyle").animate({opacity:1},3500);
        $("#cartoonstyle").animate({opacity:0},3500);
    });
    $(".style4").click(function(){
        $("#normalstyle").animate({opacity:0},3500);
        $("#watercolorstyle").animate({opacity:0},3500);
        $("#sketchstyle").animate({opacity:0},3500);
        $("#pixelstyle").animate({opacity:0},3500);
        $("#cartoonstyle").animate({opacity:1},3500);
    });
});
$(".surveybutton").hide();
$(".lastroom1").hide();
$("#surveylogo").hide();
$("#survey").hide();
$("#arrow3").hide();
$(document).ready(function(){
    $(".lightbutton3").click(function(){
        $("#bglight5").animate({opacity:1},3000);
        $("#surveylogo").show(2000);
        $(".surveybutton").show(2000);
        $(".lightbutton3").remove();
    });
    $(".surveybutton").click(function(){
        $("#survey").show(2000);
        $(".lastroom1").show(2000);
        $("#arrow3").show(2000);
    });
    $("#ok").click(function(){
        $("#error").hide(100);
        $("#thank").hide(100);
        $("#ok").hide(100);
    });
    $(".lastroom1").click(function(){
        $(location).attr("href","#fourthpage");
    });
    $("#submit").click(function(){
        $("#error").show();
        $("#thank").show();
        $("#ok").show();
    });
});
